<?php
include "controllers/config.php";
/* session_start(); */
if (isset($_POST["lang"])) {
    $lang = $_POST["lang"];
    if (!empty($lang)) {
        $_SESSION["lang"] = $lang;
    }
}

if (isset($_SESSION["lang"])) {
    $lang = $_SESSION["lang"];
    require "lang/" . $lang . ".php";
} else {
    require "lang/en.php";
}

/* $title = $lang["title_home"]; */
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="This 2020, the 14th Los Cabos VIP Summit will be virtual and will incorporate an exclusive day for travel advisors.">
    <title>Los Cabos - Vipsummit</title>

    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/6b6f9f9a36.js" crossorigin="anonymous"></script>

    <!-- bootstrap cnd -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!-- popup -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" />

    <!-- Slick carousel -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />

    <!-- Custom styles -->
    <link rel="stylesheet" href="<?= BASE_PATH ?>resources/css/main.css?v1=534634">
</head>

<body class="home">

    <header id="header">
        <div class="header-container">
            <div class="img-container">
                <a href="<?= BASE_PATH ?>">
                    <img src="<?= BASE_PATH ?>public/img/logos/Landing_Page_VS_2020.png" alt="Los Cabos VIP Summit Logo">
                </a>
            </div>
            <div class="translate-buttons">
                <form method="POST">
                    <button class="translate-button" name="lang" value=<?= $lang["trans_en_lang_min"] ?> type="submit"><?= $lang["trans_en_lang"] ?></button>
                    <span class="translate-divisor">
                        |
                    </span>
                    <button class="translate-button" name="lang" value=<?= $lang["trans_es_lang_min"] ?> type="submit"><?= $lang["trans_es_lang"] ?></button>
                </form>
            </div>
        </div>
    </header>

    <main id="home">
        <section class="principal-container">
            <div class="parallax-effect">
            </div>
            <div class="head-description">
                <div>
                    <div class="blue-container">
                        <div class="text-container">
                            <p class="date-container">
                                <?= $lang["date_desc_container"] ?>
                            </p>
                            <p class="title-vipsummit">
                                <?= $lang["title_desc_container"] ?>
                            </p>
                            <p class="desc-container">
                                <?= $lang["text_desc_container"] ?>
                            </p>
                        </div>
                        <div class="wave-container">
                            <div class="img-container">
                                <img src="<?= BASE_PATH ?>public/img/home/textures/Textura_olas.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="welcome-container">
            <div class="welcome-content">
                <h2>
                    <?= $lang["title_welcome"] ?>
                </h2>
                <p class="gold-text">
                    <?= $lang["subtitle_welcome"] ?>
                </p>
                <p>
                    <?= $lang["paragraph_welcome1"] ?>
                </p>
                <p>
                    <?= $lang["paragraph_welcome2"] ?>
                </p>
                <p class="blue-text">
                    <?= $lang["bottom_subtitle_welcome"] ?>
                </p>
            </div>
            <div class="message-partners">
                <h2>
                    <?= $lang["message_partners_title"] ?>
                </h2>
                <div class="video-carousel">
                    <div class="partners-videos">
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468283288?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Victor_Mayo.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Victor Mayo
                                        </p>
                                        <p class="occupation-person-video">
                                            Director of Product & Partnerships
                                        </p>
                                        <p class="company-person-video">
                                            Flight Center
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468283288?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468283204?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Sean_Taylor.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Sean Taylor
                                        </p>
                                        <p class="occupation-person-video">
                                            Head of Creative
                                        </p>
                                        <p class="company-person-video">
                                            Black Tomato
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468283204?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468282826?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Carlos_Garcia.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Carlos García
                                        </p>
                                        <p class="occupation-person-video">
                                            Marketing Director
                                        </p>
                                        <p class="company-person-video">
                                            Logitravel
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468282826?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468282852?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/David_Hu.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            David Hu
                                        </p>
                                        <p class="occupation-person-video">
                                            President
                                        </p>
                                        <p class="company-person-video">
                                            Classic Vacation
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468282852?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468282899?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Jack_Richards.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Jack Richards
                                        </p>
                                        <p class="occupation-person-video">
                                            President & CEO
                                        </p>
                                        <p class="company-person-video">
                                            Pleasant Holidays
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468282899?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468283028?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Jennifer_Ponton.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Jennifer Ponton
                                        </p>
                                        <p class="occupation-person-video">
                                            Former Regional Director Mexico & Caribbean
                                        </p>
                                        <p class="company-person-video">
                                            OTS Globe
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468283028?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468283026?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Matthew_Upchurch.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Matthew Upehureh
                                        </p>
                                        <p class="occupation-person-video">
                                            Chief Executive Officer
                                        </p>
                                        <p class="company-person-video">
                                            Virtuoso
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468283026?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="carousel-video-container">
                                <div class="img-container">
                                    <a href="https://player.vimeo.com/video/468283148?autoplay=true">
                                        <img src="<?= BASE_PATH ?>public/img/home/slide_video/Maurice_Bonham.jpg" alt="">
                                        <span class="play-fade">
                                            <span>
                                                <i class="fas fa-play-circle"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                                <div class="text-container">
                                    <div class="desc-video-container">
                                        <p class="name-person-video">
                                            Mauriee Bonham-Carter
                                        </p>
                                        <p class="occupation-person-video">
                                            President & CEO
                                        </p>
                                        <p class="company-person-video">
                                            ID travel
                                        </p>
                                    </div>
                                    <div class="btn-open-container">
                                        <p>
                                            <a href="https://player.vimeo.com/video/468283148?autoplay=true">
                                                <?= $lang["view_video"] ?> <i class="fas fa-chevron-right"></i>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcome-new-experience">
                <div class="parallax-effect">
                    <div>
                        <p>
                            <?= $lang["parallax_banner_text"] ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="register-today">
            <div>
                <div class="text-container">
                    <p>
                        <span class="line-one d-inline d-lg-block">
                            <?= $lang["be_part_new_chapter1"] ?>
                        </span>
                        <span class="line-two d-inline d-lg-block">
                            <?= $lang["be_part_new_chapter2"] ?>
                        </span>
                        <span class="line-three d-inline d-lg-block">
                            <?= $lang["be_part_new_chapter3"] ?>
                        </span>
                    </p>
                </div>
                <div class="images-register">
                    <div class="row">
                        <div class="col-6">
                            <div class="img-container">
                                <img src="<?= BASE_PATH ?>public/img/home/Destino_3.png" alt="">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="img-container">
                                <img src="<?= BASE_PATH ?>public/img/home/Destino_1.png" alt="">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="img-container">
                                <img src="<?= BASE_PATH ?>public/img/home/Destino_2.png" alt="">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="img-container">
                                <img src="<?= BASE_PATH ?>public/img/home/textures/Textura_2.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="register-container">
                                <p>
                                    <?= $lang["register_today"] ?>
                                </p>
                                <form>
                                    <!-- <div class="form-group">
                                        <select class="form-control register_type" id="exampleFormControlSelect1">
                                            <option value="1"><?= $lang["register_type1"] ?></option>
                                            <option value="2"><?= $lang["register_type2"] ?></option>
                                            <option value="3"><?= $lang["register_type3"] ?></option>
                                        </select>
                                    </div> -->
                                    <!-- <button type="submit" class="btn btn-primary mb-2"><?= $lang["register_btn"] ?></button> -->
                                    <div class="btn-register">
                                        <a href="https://travalliancemedia.6connex.com/event/2020loscabos/login" target="_blank">
                                            <?= $lang["register_btn"] ?>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="background-blue"></div>
        </section>
        <section class="diary-section">
            <div>
                <h2><?= $lang["agenda_title"] ?></h2>
                <p><?= $lang["agenda_times"] ?></p>
                <div class="diary-table-container">
                    <table class="table d-table d-md-none">
                        <thead>
                            <tr>
                                <th class="orange-background" scope="col"><?= $lang["agenda_category1"] ?></th>
                            </tr>
                            <tr>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day1"] ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    10:00 - 10:15<br>
                                    <span class="bold-cell"><?= $lang["agenda_day1_time1"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    10:15 - 10:45<br>
                                    <span class="bold-cell"><?= $lang["agenda_day1_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    11:00 - 17:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day2"] ?></th>
                            </tr>
                            <tr>
                                <td>
                                    10:30 - 11:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time1"] ?></span>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    11:00 - 17:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th class="orange-background" scope="col"><?= $lang["register_type2"] ?></th>
                            </tr>
                            <tr>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day3"] ?></th>
                            </tr>
                            <tr>
                                <td>
                                    11:00 - 14:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time1"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    14:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time3"] ?></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table d-none d-md-table">
                        <thead>
                            <tr>
                                <th class="orange-background" scope="col" colspan="2"><?= $lang["agenda_category1"] ?></th>
                                <th class="orange-background" scope="col" colspan="1"><?= $lang["register_type2"] ?></th>
                            </tr>
                            <tr>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day1"] ?></th>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day2"] ?></th>
                                <th class="blue-background" scope="col"><?= $lang["agenda_day3"] ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    10:00 - 10:15<br>
                                    <span class="bold-cell"><?= $lang["agenda_day1_time1"] ?></span>
                                </td>
                                <td>
                                    10:30 - 11:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time1"] ?></span>
                                </td>
                                <td>
                                    11:00 - 14:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time1"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    10:15 - 10:45<br>
                                    <span class="bold-cell"><?= $lang["agenda_day1_time2"] ?></span>
                                </td>
                                <td>
                                    11:00 - 17:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time2"] ?></span>
                                </td>
                                <td>
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    11:00 - 17:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day2_time2"] ?></span>
                                </td>
                                <td>
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                                <td>
                                    14:00<br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time3"] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <?= $lang["agenda_on_demand"] ?><br>
                                    <span class="bold-cell"><?= $lang["agenda_day3_time2"] ?></span>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <section class="gallery-section">
            <h2><?= $lang["gallery_title"] ?></h2>
            <div class="gallery-images">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-6">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up1.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_1.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up2.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up6.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_6.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up5.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_5.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up4.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_4.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="img-container">
                                    <a href="<?= BASE_PATH ?>public/img/home/gallery/Galeria_Pop_Up3.jpg">
                                        <img src="<?= BASE_PATH ?>public/img/home/gallery/Galeria_3.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="popup-video hide">
            <div class="popup-video-container">
                <div class="responsive-video" style="text-align: center;">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" allow="autoplay" src=""></iframe>
                    </div>
                </div>
            </div>
            <div class="fade-background">
                <a class="closeVideo" href="#">
                    <i class="fas fa-times"></i>
                </a>
            </div>
        </div>
    </main>

    <footer id="footer">
        <div id="topButton" class="top-btn">
            <a href="#">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
        <div class="footer-container">
            <div class="footer-top-section">
                <p>
                    <?= $lang["footer_contact"] ?>
                </p>
                <p>
                    <a href="mailto:info@loscabosvipsummit.com">info@loscabosvipsummit.com</a>
                </p>
            </div>
            <div class="footer-bottom-section">
                <p>
                    <?= $lang["footer_follow"] ?>
                </p>
                <div class="social-media-container">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/LosCabosTourism/" target="_blank">
                                <span class="diamond-container-icon">
                                    <i class="fab fa-facebook-f"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/loscabostourism/" target="_blank">
                                <span class="diamond-container-icon">
                                    <i class="fab fa-instagram"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/LosCabosTourism" target="_blank">
                                <span class="diamond-container-icon">
                                    <i class="fab fa-twitter"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/user/loscabostourism" target="_blank">
                                <span class="diamond-container-icon">
                                    <i class="fab fa-youtube"></i>
                                </span>
                            </a>
                        </li>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/loscabostourismboard/" target="_blank">
                                <span class="diamond-container-icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    <script src="<?= BASE_PATH ?>resources/js/main.js?v1=346346"></script>

</body>