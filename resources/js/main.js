//top smooth button
$("#topButton").click(function(e) {
    e.preventDefault();
    $("body, html").animate({
        scrollTop: 0
    }, 1000);
});

$(window).scroll(function() {
    if ($(window).scrollTop() > 100) {
        $('#topButton').removeClass('hidden');
        $('#topButton').addClass('show');
    } else if ($(window).scrollTop() < 100) {
        $('#topButton').removeClass('show');
        $('#topButton').addClass('hidden');
    }
});

//Carousel Videos
$('.partners-videos').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: false
            }
        }
    ]
});


//popup images gallery
$('.gallery-images').magnificPopup({
    delegate: 'a', // child items selector, by clicking on it popup will open
    type: 'image',
    gallery: { enabled: true }
    // other options
});

//custom script popup video
$('.partners-videos .carousel-video-container a').on('click', function (e) {
    e.preventDefault();
    //var srcVideo = $(this)[0].href;
    var srcVideo = $(this).attr('href');
    $('.responsive-video .embed-responsive iframe')[0].src = srcVideo;
    $('.popup-video').removeClass('hide');
})

$('.closeVideo').on('click', function (e) {
    closeVideo(e);
    $('.responsive-video .embed-responsive iframe')[0].src = "";
});

/* $('.fade-background').on('click', function (e) {
    closeVideo(e);
}); */


/* $(document).on('keyup', function (e) {
    closeVideo(e);
}); */

function closeVideo(ev) {
    ev.preventDefault();
    $('.popup-video').addClass('hide');
    //lightBoxVideo.pause();
}

//select script
/* $('.register_type').on('change', function (e) {
    var elementSelected = $('.register_type').val();
    var linksRegister = ["https://www.facebook.com/LosCabosTourism/","https://www.instagram.com/loscabostourism/","https://twitter.com/LosCabosTourism"]
    switch (elementSelected) {
        case "1":
            $('.btn-register a').attr("href", linksRegister[0]);
            console.log($('.btn-register a'));
            console.log(linksRegister[0]);
            break;
        case "2":
            $('.btn-register a').attr("href", linksRegister[1]);
            break;
        case "3":
            $('.btn-register a').attr("href", linksRegister[2]);
            break;
    }
}); */