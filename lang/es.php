<?php

$lang = array(

    "trans_en_lang_min" => "en",
    "trans_en_lang" => "EN",
    "trans_es_lang_min" => "es",
    "trans_es_lang" => "ES",

    //home
    "date_desc_container" => "NOVIEMBRE 9-11",
    "title_desc_container" => "14<sup><u>o</u></sup> VIP SUMMIT VIRTUAL DE LOS CABOS",
    "text_desc_container" => "Este 2020, la edición no. 14 del Los Cabos VIP Summit será virtual e incorporará un día exclusivo para agentes de viaje. Construye y consolida nueva alianzas y negociaciones en el destino con el mayor crecimiento de México.",
    "title_welcome" => "Bienvenido",
    "subtitle_welcome" => "Aparta la fecha y participa en el 14<sup><u>o</u></sup> VIP Summit Virtual de Los Cabos",
    "paragraph_welcome1" => "Acompáñanos en la edición virtual de Los Cabos VIP Summit y celebra la continuidad de nuestras alianzas. Al tiempo que abrimos las puertas a una nueva realidad, estamos implementando una plataforma online para forjar nuevas alianzas reuniendo a los mejores profesionales de la industria turística de alrededor del mundo a través de un programa de reuniones uno a uno. ",
    "paragraph_welcome2" => "Este año, el evento contempla un programa educativo virtual, disponible en todo momento durante el evento, diseñado para los socios comerciales, hoteles  y prestadores de servicios locales con oradores de alto nivel, líderes de opinión y expertos en la materia. Además de innovar con esta plataforma digital, el evento de este año destacará por contar con un Trade Show para agentes de viaje, en el cual podrán interactuar en vivo con los hoteles y prestadores de servicios locales.",
    "bottom_subtitle_welcome" => "¡Prepárate para experimentar un destino: el destino de tu imaginación!",
    "message_partners_title" => "Un mensaje de <br>nuestros compañeros",
    "view_video" => "Ver Video",
    "parallax_banner_text" => "Bienvenidos a una nueva <br>experiencia en Los Cabos.",
    "be_part_new_chapter1" => "SÉ PARTE DE ESTE ",
    "be_part_new_chapter2" => "NUEVO CAPÍTULO ",
    "be_part_new_chapter3" => "PARA LOS CABOS",
    "register_today" => "¡Regístrate hoy!",
    "register_type1" => "Socios comerciales",
    "register_type2" => "Agentes de viaje",
    "register_type3" => "Hoteles y prestadores",
    "register_btn" => "Registrar",
    "agenda_title" => "Agenda",
    "agenda_times" => "Nota: horario local de Los Cabos",
    "agenda_category1" => "Socios y proveedores de destino",
    "agenda_category2" => "Asesores de viajes",
    "agenda_day1" => "Lunes, 9 de Noviembre",
    "agenda_day1_time1" => "Mensaje de Inauguración",
    "agenda_day1_time2" => "Conferencia Magistral",
    "agenda_day1_time3" => "Reuniones de Negocios",
    "agenda_day1_time4" => "Programa Educativo",
    "agenda_day2" => "Martes, 10 de Noviembre",
    "agenda_day2_time1" => "Panel de Expertos",
    "agenda_day2_time2" => "Reuniones de Negocios",
    "agenda_day2_time3" => "Programa Educativo",
    "agenda_day3" => "Miércoles, 11 de Noviembre",
    "agenda_day3_time1" => "Trade Show para Agentes de Viaje",
    "agenda_day3_time2" => "Programa Educativo",
    "agenda_day3_time3" => "Evento Virtual de Clausura",
    "agenda_on_demand" => "Bajo Demanda",
    "gallery_title" => "Galería",

    //footer

    "footer_contact" => "Para más información contáctanos:",
    "footer_follow" => "Síguenos",

);
