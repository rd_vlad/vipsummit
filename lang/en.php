<?php

$lang = array(

    "trans_en_lang_min" => "en",
    "trans_en_lang" => "EN",
    "trans_es_lang_min" => "es",
    "trans_es_lang" => "ES",

    //home
    "date_desc_container" => "NOVEMBER 9-11",
    "title_desc_container" => "VIRTUAL LOS CABOS VIP SUMMIT",
    "text_desc_container" => "This 2020, the 14th Los Cabos VIP Summit will be virtual and will incorporate an exclusive day for travel advisors. Experience building new alliances and negotiations in the fastest growing destination in Mexico.",
    "title_welcome" => "Welcome",
    "subtitle_welcome" => "Mark your Calendar for the Virtual 14th Los Cabos VIP Summit.",
    "paragraph_welcome1" => "Please join us in this year's virtual edition of the Los Cabos VIP Summit, and celebrate the continuation of our valued partnerships. As we open the door to a new reality, we are now using an online platform to forge new alliances while bringing together top travel professionals from around the world through a one-on-one business meeting program.",
    "paragraph_welcome2" => "This year's event will also consider a virtual On Demand educational program designedfor partners, and local suppliers with top speakers, thought leaders and field experts.Apart from innovating with a digital platform, this year's event will feature an exclusiveday for a travel agent trade show where you will be able to talk and interact live with the wide array of destination suppliers.",
    "bottom_subtitle_welcome" => "Get ready to experience the destination of your imagination!",
    "message_partners_title" => "A message from <br>our Partners",
    "view_video" => "View Video",
    "parallax_banner_text" => "Welcome to a new kind <br>of experience in Los Cabos.",
    "be_part_new_chapter1" => "BE PART OF THIS ",
    "be_part_new_chapter2" => "NEW CHAPTER ",
    "be_part_new_chapter3" => "FOR LOS CABOS",
    "register_today" => "Register today!",
    "register_type1" => "Partner registration",
    "register_type2" => "Travel advisors",
    "register_type3" => "Destination supppliers",
    "register_btn" => "Register",
    "agenda_title" => "Agenda",
    "agenda_times" => "All times are MST",
    "agenda_category1" => "Partners and Destination Suppliers",
    "agenda_category2" => "Travel Advisors",
    "agenda_day1" => "Monday, November 9<sup>th</sup>",
    "agenda_day1_time1" => "Welcome Message",
    "agenda_day1_time2" => "Keynote Speech",
    "agenda_day1_time3" => "1 on 1 Business Meetings",
    "agenda_day1_time4" => "Educational Program",
    "agenda_day2" => "Tuesday, November 10<sup>th</sup>",
    "agenda_day2_time1" => "Experts Panel",
    "agenda_day2_time2" => "1 on 1 Business Meetings",
    "agenda_day2_time3" => "Educational Program",
    "agenda_day3" => "Wednesday, November 11<sup>th</sup>",
    "agenda_day3_time1" => "Travel Agent Trade Show",
    "agenda_day3_time2" => "Educational Program",
    "agenda_day3_time3" => "Virtual Closing Event",
    "agenda_on_demand" => "On Demand",
    "gallery_title" => "Gallery",

    //footer

    "footer_contact" => "For further information, please contact:",
    "footer_follow" => "Follow Us",
);
